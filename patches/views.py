from django.http import HttpResponse
from django.shortcuts import render

from .models import Patch

def latest_patches(request):
    patches = Patch.objects.all().order_by('-created_on')

    for patch in patches:
        patch.display_name = patch.name or patch.filename

    return render(request, 'patches.html',
        {'patches': patches}
    )

def free_download(request, patch_id:int):
    patch = Patch.objects.get(id=patch_id)
    response =  HttpResponse(patch.patch_file, content_type='application/octet-stream')
    response['Content-Disposition'] = f'attachment; filename="{patch.filename}"'
    return response