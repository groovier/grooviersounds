from django.contrib import admin

from .models import Patch

admin.site.register(Patch)
