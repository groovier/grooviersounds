from os.path import basename

from django.db import models
from django.contrib.auth.models import User

def user_directory_path(instance, filename):
    # files will be uploaded to MEDIA_ROOT
    return '/'.join(('user_' + str(instance.owner.id), 'patch_files', filename))

class Patch(models.Model):
    owner = models.ForeignKey(
        User,
        models.CASCADE
    )

    created_on = models.DateField(
        auto_now_add=True
    )

    name = models.CharField(max_length=20)

    patch_file = models.FileField(
        upload_to=user_directory_path
    )

    class Meta:
        verbose_name_plural = 'patches'
        constraints = [
            models.UniqueConstraint(fields=['owner', 'name'], name='owner_plus_name_unique'),
        ]
    
    @property
    def filename(self):
        return basename(self.patch_file.name)

    def __str__(self):
        return '/'.join((self.owner.username, self.filename))