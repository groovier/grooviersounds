from django.urls import path

from . import views

urlpatterns = [
    path('free_download/<int:patch_id>', views.free_download, name='free_download'),
]