from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import login, logout

from .forms import UserForm, LoginForm

def account(request):
    if request.user.is_authenticated:
        return render(request, 'your_account.html', {'user': request.user })

    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = UserForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # Create the usr object and log the user in
            new_user = form.save()
            login(request, new_user)
            # redirect to a new URL:
            return HttpResponseRedirect('/account/welcome/')

    # if a GET (or any other method) we'll create a blank form
    else:
        create_form = UserForm()
        login_form = LoginForm()

    return render(request, 'login_or_create.html', {
        'create_form': create_form,
        'login_form': login_form
        })

def welcome(request):
    if request.user.is_authenticated:
        first_name = request.user.first_name
        return render(request, 'welcome.html', {'first_name': first_name})

    else:
        return HttpResponseRedirect('/')

def logout_user(request):
    if request.user.is_authenticated:
        first_name = request.user.first_name
        logout(request)
        return render(request, 'goodbye.html', {'first_name': first_name})

    else:
        return HttpResponseRedirect('/')