from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['email', 'first_name', 'last_name', 'username', 'password']
        widgets = {
            'password': forms.PasswordInput(),
        }
    
    def clean_email(self):
        data = self.cleaned_data['email']
        existing_users = User.objects.filter(email=data)
        if existing_users:
            raise ValidationError(
        "An account with this email address already exists. " +
        "If you do not know your password, please contact the system administrator.")

        return data


class LoginForm(forms.Form):
    email = forms.EmailField(label='Email address')
    password = forms.CharField(max_length=20)